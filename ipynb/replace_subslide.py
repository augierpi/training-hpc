
from pathlib import Path

bad_line = '     "slide_type": "subslide"'
better_line = '     "slide_type": "slide"'

paths = Path(".").glob("*.ipynb")

for path in paths:
    with open(path) as file:
        txt = file.read()

    print(path)

    if bad_line not in txt:
        continue

    lines = txt.split("\n")
    lines_new = []
    for line in lines:
        if line == bad_line:
            line = better_line
        lines_new.append(line)

    txt_new = '\n'.join(lines_new)

    with open(path, "w") as file:
        file.write(txt_new)
