#!/usr/bin/env python3

from functools import partial
from runpy import run_path
from pathlib import Path
import dask
from dask import delayed
from dask.distributed import Client
import numpy as np
from dtw_cort import cort, dtw_distance
util = run_path(Path(__file__).absolute().parent.parent / "util.py")


def serie_pair_index_generator(number):
    """ generator for pair index (i, j) such that i < j < number

    :param number: the upper bound
    :returns: pairs (lower, greater)
    :rtype: a generator
    """
    return (
        (_idx_greater, _idx_lower)
        for _idx_greater in range(number)
        for _idx_lower in range(_idx_greater)
    )


def distances(series, idx_s1, idx_s2, res_dtw, res_cort):
    """Computes the distances (dtw and cort) of series s1, and s2
        and puts result in res_mat_dtw and res_mat_cort

    :series: (np array) series
    :idx_s1:  index of first serie in series
    :idx_s2: index of second serie in series
    :res_sult:  (tuple) idx_s1, idx_s2, dtw and cort between series[s1] and series[s2]
    """
    dist_dtw = dtw_distance(series[idx_s1], series[idx_s2])
    dist_cort = 0.5 * (1 - cort(series[idx_s1], series[idx_s2]))
    res_dtw[idx_s1, idx_s2] = dist_dtw
    res_dtw[idx_s2, idx_s1] = dist_dtw
    res_cort[idx_s1, idx_s2] = dist_cort
    res_cort[idx_s2, idx_s1] = dist_cort
    return True


def compute(series, nb_series):
    client = Client(threads_per_worker=4, n_workers=1)
    gen = serie_pair_index_generator(nb_series)
    _dist_mat_dtw = np.zeros((nb_series, nb_series), dtype=np.float64)
    _dist_mat_cort = np.zeros((nb_series, nb_series), dtype=np.float64)
    inputs = ((series, t1, t2) for t1, t2 in gen)
    res = [delayed(distances)(s, t1, t2, _dist_mat_dtw, _dist_mat_cort)
           for s, t1, t2 in inputs]
    client.compute(res)
    return _dist_mat_dtw, _dist_mat_cort


main = partial(util["main"], compute)

if __name__ == "__main__":
    main()
